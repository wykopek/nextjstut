module.exports = (phase, {defaultConfig}) => {
    /**
     * @type {import('next').NextConfig}
     */
    return {
        env: {
            API_URL: process.env.API_URL
        }
    }
}
