Struktura katalogów:

    /app - katalog z plikami aplikacji
        /components - katalog z komponentami aplikacji
        /pages - katalog z plikami page.js, odpowiedzialnymi za renderowanie poszczególnych stron
        /public - katalog z plikami, które będą dostępne bezpośrednio pod podanym adresem URL
        /styles - katalog z plikami arkuszy stylów CSS
        _app.js - plik z konfiguracją layoutu głównego aplikacji
        _document.js - plik z konfiguracją dokumentu HTML
        api.js - plik z funkcjami obsługującymi zapytania do serwera
    /server - katalog z plikami serwera aplikacji
        server.js - plik z kodem serwera aplikacji
    package.json - plik z opisem projektu i jego zależnościami
    next.config.js - plik z konfiguracją frameworka Next.js
    