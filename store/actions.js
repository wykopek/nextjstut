debugger;
export const setUser = user => ({
    type: 'SET_USER',
    user
})

export const setProducts = products => ({
    type: 'SET_PRODUCTS',
    products
})