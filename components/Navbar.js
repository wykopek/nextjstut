debugger;
import Link from 'next/link'
import { useRouter } from 'next/router'
const Navbar = () => {
const { pathname } = useRouter()
    return (
            <nav>
                <Link href="/" className={pathname === '/' ? 'active' : ''}>Home</Link>
                <Link href="/products" className={pathname === '/products' ? 'active' : ''}>Products</Link>
                <Link href="/cart" className={pathname === '/cart' ? 'active' : ''}>Cart</Link>
                <Link href="/account" className={pathname === '/account' ? 'active' : ''}>Account</Link>
            </nav>
            )
}

export default Navbar
