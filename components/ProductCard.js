debugger;
import {useRouter} from 'next/router'

const ProductCard = ({product}) => {
    const router = useRouter()

    const handleClick = () => {
        router.push({
            pathname: `/product/${product.id}`
//            query: { id: product.id }
        })
    }

    return (
            <div onClick={handleClick}>
                <h2>{product.name}</h2>
                <p>{product.description}</p>
                <p>{product.price}</p>
            </div>
    )
}

export default ProductCard
