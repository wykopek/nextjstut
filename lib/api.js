import axios from 'axios'

const api = axios.create({
    baseURL: process.env.API_URL
})

export const fetchUser = async (refresh = false) => {
    return callBackend(refresh, '/api/user', 'user') || []
}

export const fetchProducts = async (refresh = false) => {
    return callBackend(refresh, '/api/products', 'all_products') || []
}

export const getProductById = async (id, refresh = false) => {
    return callBackend(refresh, `/products/${id}`, `product_${id}`) || {}
}

const callBackend = async (forceRefresh = false, url, objectName) => {
    const storageObject = localStorage.getItem(objectName);
    if (!forceRefresh && storageObject) {
        return JSON.parse(storageObject);
    }
    try {
        const objResponse = api.get(url);
        if (objResponse.ok) {
            const obj = await response.json();
            localStorage.setItem(objectName, JSON.stringify(obj));
            return obj;
        }
    } catch (error) {
        console.error(`Error fetching object type '${objectName} with url: ${url}, error: ${error}`, error);
    }
    return null
}
