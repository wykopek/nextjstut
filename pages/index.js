debugger;
import {useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux'

import ProductCard from '../components/ProductCard'
import {fetchProducts} from '../lib/api'
import {setProducts} from '../store/actions'

const fetchData = async () => {
    const fetchedProducts = await fetchProducts()
//    dispatch(setProducts(fetchedProducts))
    console.log(`fetched products: ${fetchedProducts}`)
}
console.log("running, index...")

const Home = () => {
//    const dispatch = useDispatch()
    debugger;
    const dispatch = ""
    const products = [{id: "haha", name: "product name :)", description: "product desc", price:"21.37"}] // useSelector(state => state.products)

    useEffect(() => {
        fetchData()
    }, [dispatch])

    return (
            <div>
                {products.map(product => (
                        <ProductCard key={product.id} product={product}/>
                ))}
            </div>
    )
}

export default Home
