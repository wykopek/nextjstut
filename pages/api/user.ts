import type { NextApiHandler } from 'next'

const mockUser = {
    id: 1,
    name: 'John Doe',
    email: 'john@example.com'
};
const handler: NextApiHandler = async (req, res) => {
    return res.status(200).json(mockUser)
}

export default handler