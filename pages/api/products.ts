import type { NextApiHandler } from 'next'

// Mock product data
const mockProducts = [
    { id: 1, name: 'Product 1' },
    { id: 2, name: 'Product 2' },
    { id: 3, name: 'Product 3' }
];

const handler: NextApiHandler = async (req, res) => {
    const { body } = req;
    // body: { name: 'Lazar Nikolov', profession: 'Software Engineer' }
    if (req.method === 'GET') {
        // process the GET request
        res.json(mockProducts);
    }
    if (req.method === 'POST') {
        // save data (body) in database
    }
}
export default handler