debugger;
import { useEffect } from 'react'
import { useRouter } from 'next/router'
import { useDispatch } from 'react-redux'
import { fetchUser } from '../lib/api'
import { setUser } from '../store/actions'
import { Provider } from 'react-redux';
import { store } from '../redux/store';
import Navbar from '../components/Navbar'

//const dispatch = useDispatch()

const fetchData = async () => {
    const user = await fetchUser()

//    const dispatch = useDispatch()
//    dispatch(setUser(user))
}

const App = ({ Component, pageProps }) => {

//    const dispatch = useDispatch()
    debugger;
const dispatch = "asdf"
    useEffect(() => {
        fetchData()
    }, [dispatch])

    return (
            <Provider store={ store }>
                <Navbar />
                <Component { ...pageProps } />
            </Provider>
            )
}

export default App
