debugger;
import { useRouter } from 'next/router'
import ProductCard from '../../components/ProductCard'
import { getProductById } from '../../lib/api'

const Product = ({ product }) => {
    return (
            <div>
                <ProductCard product={product} />
            </div>
            )
}

/*Product.getInitialProps = async ({ query }) => {
    const { id } = query
    const product = await getProductById(id)
    return { product }
}*/
Product.getInitialProps = async () => {
    const router = useRouter()
    const { id } = router.query
    const product = await getProductById(id)
    return { product }
}

export default Product
